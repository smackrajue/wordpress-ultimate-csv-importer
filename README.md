# Ultimate CSV Importer

Simple, powerful and easy CSV Import plugin even for newbies and novice users.

## Description
The only well managed, supported and regularly updated importer plugin with great track record for wordpress. Manage your WordPress content as simple as in a spreadsheet editor like Microsoft Excel or Openoffice, etc,.

[Ultimate CSV Importer](https://www.smackcoders.com/wp-ultimate-csv-importer-pro.html?utm_source=web&utm_campaign=readme&utm_medium=bitbucket) is an import tool that helps to build a WordPress site at the drop of a hat by importing Post, Page, Custom Post, Comments, WordPress Custom Fields. Reviews of WP Customer Reviews and SEO fields of All in One SEO. The plugin supports import of all Custom Post Types. 

The image can be imported from external URL like Google image, pixabay, shutterstock, etc,. The images are imported in the background with the help of WP-CRON to enhance the performance. You can [easily configure your WP-Cron](https://www.smackcoders.com/blog/enable-configure-wp-cron.html?utm_source=web&utm_campaign=bitbucket&utm_medium=readme) for faster import.

### Modules supported in Import 
* Core Modules - Post, Page, Custom Post, Comments.
* Users - [Import Users](https://wordpress.org/plugins/import-users/) add-on to import your WordPress Users
* Custom Post - CPT UI, CustomPress and default WordPress Custom Post.	
* Custom Field - WordPress default Text and Text area fields.
* SEO field - Free version of All in One SEO.
* Reviews - WP Customer Reviews

### Highlighted Feature =
* High performance rocket speed import.
* Imports images from an external URLs mentioned in CSV files.
* Import with enhanced duplicate handling to optimize your database.
* Free add-on to [Import Users](https://wordpress.org/plugins/import-users/) to upload user info into WordPress 
* WordPress core custom fields can be dynamically registered on the flow of CSV import.
* Post type import with terms & taxonomies with any of depth of parent-child values.
* WP Ultimate CSV Importer can be used in WordPress multisite.
* Post Type CSV import along with multi category & multi tag.
* Import CSV with any delimiter in UTF-8 format can be imported.
* Free export add-on to [export](https://wordpress.org/plugins/wp-ultimate-exporter/) all your WP content in CSV file.

### Note 
* Featured image from password protected image location can't be imported.
* All languages supported by WordPress can be imported in UTF-8 without BOM format.

https://www.youtube.com/watch?v=Uho22R0BCHQ&feature=youtu.be

## Ultimate CSV Importer PRO
CSV Importer PRO has all the features of WP Ultimate CSV Importer and it is extended with a lot of features that enables to import, schedule(recurring import), update and export CSV files. All import details over a year is in dashboard chart to easily track the WordPress site updates in a fraction of a second. Any changes like modifying existing WP content or appending new content can be done in simple update flow. WP Ultimate CSV Importer can also update or import the WP content from any CSV file periodically with scheduler. The update or Scheduled import (recurring import) is done with the templates which is created with mapped fields in the mapping section of WP Ultimate CSV importer. The export option enables to export the imported modules as CSV, XLS, XML and JSON based on the filters.

Import of CSV involves few simple steps. Choose your CSV file to import. Map your CSV fields with WP fields with two different mapping Advanced Mode (Automap fields, if your CSV header same as that of WP header) or Drag & Drop Mode (More intuitive drag & drop mapping). Media Handling to optimize your image import. Finally, import all your CSV content in just a click inside your WordPress.

### Supported Modules To Import and Export 
* Core Module - Post, Page, Custom Post, Comments, Category, Tags, Users, Customer Reviews.
* eCommerce Products - WooCommerce, MarketPress, WP e-Commerce & eShop.
* SEO field - Free & Pro version of All in One SEO and Yoast SEO.
* Multilingual - [WPML](https://www.youtube.com/watch?v=yvSPfvPp5s0&t=8s) & [qTranslate X](https://www.smackcoders.com/blog/import-marketpress-pro-event-manager-and-more.html?utm_source=web&utm_campaign=readme&utm_medium=wp_org)
* Image - NextGEN Gallery
* Events Management - Events Manager FREE & Pro
* Custom fields - CustomPress, [ACF](https://youtu.be/RVoqPX3Ib94), [Toolset Types](https://www.smackcoders.com/documentation/ultimate-csv-importer-pro/toolset-types-import?utm_source=readme&utm_medium=wp_org), Pods, CCTM & WordPress default Text & Textarea fields.
* Custom Post - CustomPress, Types, Pods, CPT UI, CCTM & default WordPress Custom Post.

### Highlighted Feature of PRO
* CSV Import - WP Ultimate CSV Importer support the import of any CSV or XML file content into WordPress
* Import from different location - WP Ultimate CSV Importer provides four different options (Desktop, FTP/ SFTP, URL and Server) to upload your CSV file.
* Import image - Use the images from external URL in your CSV file and import all images inside WordPress along with the advanced SEO option to add alt text with WP Ultimate CSV Importer. You can also rename the featured image loaded from external URL.
* More optimized import process - Advanced duplicate handling in WP Ultimate CSV Importer lets you to handle duplicates with any WP fields as primary field, and eliminate duplicate entries in WordPress.
* Scheduled / Recurring Import - WP Ultimate CSV Importer can check for updates and run import periodically without any manual interaction.
* Toolset Types Import - Import of any Toolset Types information in CSV into your WordPress with WP Ultimate CSV Importer. Lets you to import Toolset Types Custom fields, [Post Relation](https://youtu.be/jkkSG0DUnug), Intermediate post, Repeatable Field & [Repeater Field Group](https://youtu.be/qKPwv3Pc7tw).
* Multilingual import - Easy to run multilingual website with WP Ultimate CSV Importer with the support for the import of WPML & qTranslate X add-on.
* WP Ultimate CSV Importer supports WooCommerce product import along with 6 WooCommerce add-on.
* Import products of MarketPress Lite & PRO version add-on.
* Registers Custom Fields of ACF (FREE & Pro), Types, Pods and WordPress fields in the flow of import.
* All imported modules are managed in the Importer File manager.
* Complete import history i.e. detailed logs are maintained in the Log manager.
